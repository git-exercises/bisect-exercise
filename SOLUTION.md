First, start a git bisection:

    git bisect start

The top of `main` branch is buggy, mark it "bad":

    git bisect bad main

But we know the first implementation was good. It's this commit:

    initial implementation of foo.compute() + unit test (guaranteed GOOD)

Either copy its id or use this command to set it in a variable:

    good_commit=$(git log --format=%H --grep="initial implementation")

Mark that commit as good:

    git bisect good "$good_commit"

At this point, git knows 1 good commit and 1 bad commit.
So it checks out some commit in the middle between the good one and the bad one.
It's up to you to test if the checked out commit is good or bad:

    ./test.py

(git should have selected the "foo: use only ones" commit, which is bad)
If the test passes, run `git bisect good`. If the test fails, run `git bisect bad`.

As soon as you marked the current commit good or bad, git selects another commit for you to test.
Let's run again:

    ./test.py

(git should have selected the "foo: use multiplication" commit, which is good)
If the test passes, run `git bisect good`. If the test fails, run `git bisect bad`.

At this point, git has enough information to tell you which commit introduced the regression: "foo: use only ones".

Reset to initial state with:

    git bisect reset

Since the command to test is always `./test.py`, and it returns 0 in case of "good", and 1 in case of "bad", most of the runs can be automated with:

    git bisect run ./test.py

`git bisect run` will repeatedly call `test.py` for you and will automatically run `bisect good` or `bisect bad` because `./test.py` tells if it passed or failed.

All the steps can be repeated but running `flake8 *.py` instead of `./test.py` to find out when the linting was broken.

