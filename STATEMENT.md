In the current state of the "main" branch, the unit tests fail.
Don't try to debug what fails now, don't look at `foo.py`, only care about the tests.
Instead, find out with `git bisect` when the regression was introduced (don't use `git bisect run` yet).
Then, use `git bisect` to reset the initial branch as it was.

To run the unit tests, simply run `./test.py`.

Hints:
- the commit noted `guaranteed GOOD` should be marked with `bisect good`
- the commit at top of "main" branch should be marked with `bisect bad`

When this is done, install flake8 (for example: `pip install flake8`) and find out with `git bisect` again when the linting was broken when running `flake8 *.py`.
