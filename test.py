#!/usr/bin/env python3

import unittest

import foo


class Test(unittest.TestCase):
    def test_foo(self):
        self.assertEqual(foo.compute(), 42)


if __name__ == "__main__":
    unittest.main()
